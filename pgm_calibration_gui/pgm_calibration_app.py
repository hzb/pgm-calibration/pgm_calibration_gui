import os
import sys

from common.json_utils import read_json_file
from common.logging_utils import logger
from pgm_calibration import Ui_MainWindow
from PyQt6.QtWidgets import QApplication, QMainWindow, QMessageBox


class PGMCalibrationApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        logger.info("Application started.")
        self.setupUi(self)  # Setup Ui
        self.show()  # Show Ui
        self.init_signals_slots()  # Connect signals to slots
        self.init_tool_tips()  # Init all tool tips
        self.load_pgm_types()  # Load pgm types from config file to the combobox selection

    def init_signals_slots(self):
        # File menu items
        self.action_print.triggered.connect(self.on_print)
        self.action_print_preview.triggered.connect(self.on_print_preview)
        self.action_export_pdf.triggered.connect(self.on_export_pdf)
        self.action_exit.triggered.connect(self.on_exit_app)

        # Action menu items
        self.action_start_calibration.triggered.connect(self.on_start_calibration)

        # Plot menu items
        self.action_clear_plot.triggered.connect(self.on_clear_plot)

        # About menu items
        self.action_about_pgm_calibration.triggered.connect(
            self.on_about_pgm_calibration
        )
        self.action_about_qt.triggered.connect(self.on_about_qt)

        # Setup tab
        self.comboBox_pgm_type.currentTextChanged.connect(self.on_pgm_type_changed)
        self.pushButton_load_pgm_types.clicked.connect(self.load_pgm_types)

    def init_tool_tips(self):
        self.action_print.setToolTip("Print of calibration plot")
        self.action_print_preview.setToolTip("Print preview of calibration plot")
        self.action_export_pdf.setToolTip("Export calibration plot to pdf")
        self.action_exit.setToolTip("Close application")
        self.action_start_calibration.setToolTip("Start calibration")
        self.action_clear_plot.setToolTip("Clear plot")
        self.action_about_pgm_calibration.setToolTip(
            "Info dialog about pgm calibration"
        )
        self.action_about_qt.setToolTip("Info dialog about qt")
        self.widget_plot.setToolTip("Calibration plot")
        self.comboBox_pgm_type.setToolTip("Pgm type selection")
        self.pushButton_load_pgm_types.setToolTip(
            "Load pgm types from config file. Use if any change made in the configuration of the pgm types."
        )

    def load_pgm_types(self):
        current_dir = os.path.dirname(__file__)
        # Construct the path to file.json
        file_path = os.path.join(current_dir, "config_files", "pgm_config.json")
        items = read_json_file(file_path)
        key: str = "pgm_types"
        try:
            self.comboBox_pgm_type.addItems(items[key])
            logger.info(f"Pgm types loaded from file: {file_path}")
        except KeyError as e:
            logger.error(f"The key '{key}' not found in json config file: {file_path}")

    # File menu slots
    def on_print(self):
        logger.info("Print triggered.")

    def on_print_preview(self):
        logger.info("Print preview triggered.")

    def on_export_pdf(self):
        logger.info("Export pdf triggered.")

    def on_exit_app(self):
        self.close()

    # Action menu slots
    def on_start_calibration(self):
        logger.info("Calibration started.")

    # Plot menu slots
    def on_clear_plot(self):
        logger.info("Clear plot triggered.")

    # About menu plots
    def on_about_pgm_calibration(self):
        msg = "PGM calibration tool"
        QMessageBox.about(self, "About PGM calibration", msg)

    def on_about_qt(self):
        msg = "PGM calibration tool"
        QMessageBox.about(self, "About PGM calibration", msg)

    def on_pgm_type_changed(self, pgm_type: str):
        self.pgm_type: str = pgm_type
        logger.info(f"Pgm type changed to: {self.pgm_type}")

    # Events
    def closeEvent(self, event):
        reply = QMessageBox.question(
            self,
            "Confirm Close",
            "Are you sure you want to close this application?",
            QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No,
            QMessageBox.StandardButton.No,
        )

        if reply == QMessageBox.StandardButton.Yes:
            logger.info("Application closed.")
            event.accept()
        else:
            event.ignore()

    # def load_devices(self):
    #     try:
    #         with open("pgm_types.json", "r") as f:
    #             data = json.load(f)
    #             pgm_types = data.get("pgm_types", [])
    #             devices_text = "\n".join(devices)
    #             self.label.setText(devices_text)
    #     except FileNotFoundError:
    #         self.label.setText("Config file not found.")


app = QApplication(sys.argv)
win = PGMCalibrationApp()
sys.exit(app.exec())
