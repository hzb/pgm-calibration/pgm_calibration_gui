"""Module:      json_utils.py

Description:    Support of reading/writing from/to json file.

Usage:
                For reading: Import whole module or read_json_file function from the module
                For writing: Import whole module or write_json_file function from the module

Advice:         If you use any static type checking tool in your IDE: install types-PyYAML
                (to ensure existance of library/type/hint stubs for the yaml package)

Example data to write
    data_to_write = {"devices": ["Device1", "Device2", "Device3"]}

    file_path = "config.json"

    # Write data to JSON file
    success = write_json_file(file_path, data_to_write)
    if success:
        print("Data was successfully written to the file.")
    else:
        print("Failed to write data to the file.")

Example read data from JSON file

    data = read_json_file(file_path)
    if data:
        print("Data read from file:", data)
    else:
        print("No data could be read from the file.")
"""

import json

from common.logging_utils import logger


def read_json_file(file_path: str) -> dict:
    """
    Reads a JSON file and returns the data as a Python dictionary.

    Args:
        file_path (str): Path to the JSON file.

    Returns:
        dict: The data from the JSON file, or an empty dictionary if an error occurs.
    """
    try:
        with open(file_path, "r") as file:
            data = json.load(file)
            # logger.info("Successfully read JSON file: %s", file_path)
            return data
    except FileNotFoundError:
        logger.error("File not found: %s", file_path)
    except json.JSONDecodeError as e:
        logger.error("Error decoding JSON from file %s: %s", file_path, e)
    except Exception as e:
        logger.error(
            "Unexpected error occurred while reading file %s: %s", file_path, e
        )

    return {}


def write_json_file(file_path: str, data: dict):
    """
    Writes data to a JSON file.

    Args:
        file_path (str): Path to the JSON file.
        data (dict): Data to write to the file.

    Returns:
        bool: True if writing was successful, False otherwise.
    """
    try:
        with open(file_path, "w") as file:
            json.dump(data, file, indent=4)
            # logger.info("Successfully wrote to JSON file: %s", file_path)
            return True
    except IOError as e:
        logger.error("I/O error while writing to file %s: %s", file_path, e)
    except TypeError as e:
        logger.error("Type error while writing data to file %s: %s", file_path, e)
    except Exception as e:
        logger.error(
            "Unexpected error occurred while writing to file %s: %s", file_path, e
        )

    return False
