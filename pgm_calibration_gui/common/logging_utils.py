"""
Logging Configuration Module

This module sets up logging configuration for a Python application,
allowing logging to both a file and the console. It provides a centralized
logger object that can be imported into other modules for consistent logging
behavior across the application.

Usage:
    Import the `logger` object from this module into your application modules.
    Use the logger methods (`debug()`, `info()`, `warning()`, `error()`,
    `critical()`) to log messages at different severity levels.

Example:
    from logging_utils import logger

    def some_function():
        logger.info('Logging from module1')
        try:
            result = 1 / 0  # Simulate an error
        except ZeroDivisionError:
            logger.error('Error occurred in module1', exc_info=True)

Attributes:
    logger: A configured logger object ready for use in logging messages
            to both a file and the console.

Logging Levels:
    The logger is configured with the following logging levels:
    - DEBUG: Detailed information, typically of interest only when
             diagnosing problems.
    - INFO: Confirmation that things are working as expected.
    - WARNING: An indication that something unexpected happened, or
               indicative of some problem in the near future (e.g.,
               disk space low). The software is still working as expected.
    - ERROR: Due to a more serious problem, the software has not been
             able to perform some function.
    - CRITICAL: A serious error, indicating that the program itself
                may be unable to continue running.

Log Format:
    The logger's format is set to include:
    - Timestamp (`asctime`)
    - Log level (`levelname`)
    - Module name (`module`)
    - Function name (`funcName`)
    - Log message (`message`)
"""

import logging


def setup_logger(log_path="app.log"):
    """
    Set up logging configuration.

    Args:
    - log_path (str): Path to the log file. Default is 'app.log'.

    Returns:
    - logging.Logger: Configured logger object.
    """
    # Create a custom logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Create handlers for logging to file and to console
    file_handler = logging.FileHandler(log_path)
    file_handler.setLevel(logging.INFO)  # Change log level as needed

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)  # Change log level as needed

    # Create a formatter that includes module, function, and message
    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s"
    )
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)

    # Add the handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    return logger


# Set up a logger with default log file path 'app.log'
logger = setup_logger()
